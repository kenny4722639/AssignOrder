Instructions for the project
First: git clone https://gitlab.com/kenny4722639/AssignOrder.git

To run the project, go to application.properties on src/main/resources and put your configurations of database and email.

After, go to AssignOrderApplication class and run as a Java Project.

Then it can be tested in postman.

First add User, then Item and after that add Order or Stock in any order.

If order is created first, when creating Stock, it's going to take uncompleted orders and assign it to then, if there is more quantity on stock then on order, it's going to look for other orders and save it on them, if there is no more orders then it's going to save a new stock without the order. If the stock quantity reaches the order quantity, the order is going to be completed and an email is sended to the user who created the order.

If stock is saved first, when creating order, it's going to look for available stock to fill the order request. When it reaches the order quantity then order is completed and an email is sended to the user who created it.
