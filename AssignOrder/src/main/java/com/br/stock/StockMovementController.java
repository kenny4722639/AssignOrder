package com.br.stock;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/stock")
public class StockMovementController {
	@Autowired
	private StockMovementService stockMovementService;

	@GetMapping("/get")
	public List<StockMovement> get(@RequestParam(name = "name", required = false) String name) {
        return stockMovementService.get(name);
    }

	@PostMapping("/save")
    public StockMovement save(@RequestBody StockMovement stockMovement) {
    	return stockMovementService.save(stockMovement);
    }

	@DeleteMapping("/delete")
	public void delete(@RequestParam(name = "name", required = false) String name) {
		stockMovementService.delete(name);
	}
}
