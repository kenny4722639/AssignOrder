package com.br.stock;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.br.email.EmailService;
import com.br.order.Order;
import com.br.order.OrderRepository;
import com.br.order.OrderStatus;

@Service
public class StockMovementService {
	@Autowired
	private StockMovementRepository stockMovementRepository;
	@Autowired
	private OrderRepository orderRepository;
	@Autowired
	private EmailService emailService;

	public List<StockMovement> get(String name) {
        return StringUtils.isNotEmpty(name) ? stockMovementRepository.findByItemName(name) : stockMovementRepository.findAll();
    }

	@Transactional
    public StockMovement save(StockMovement stockMovement) {
    	stockMovement.setCreationDate(new Date(Calendar.getInstance().getTime().getTime()));
    	
        StockMovement savedStock = stockMovementRepository.save(stockMovement);
        
        checkOrder(savedStock);
        
        return savedStock;
    }

    public void delete(String itemName) {
    	List<StockMovement> stockMovement = stockMovementRepository.findByItemName(itemName);
    	if(!stockMovement.isEmpty()) {
    		stockMovementRepository.deleteAll(stockMovement);
    	}else {
    		stockMovementRepository.deleteAll();
    	}
    }

    @Transactional
	public void checkStock(Order order) {
		List<StockMovement> stockMovements = stockMovementRepository.findByItemNameAndOrderIsNull(order.getItem().getName());
		
		stockMovements.stream().forEach(stockMovement -> {
			Long stockQuantity = stockMovement.getQuantity() - order.getQuantity();
			
			if(stockQuantity == 0) {
				order.setStatus(OrderStatus.COMPLETE.getValue());
			}else if(stockQuantity > 0) {
				order.setStatus(OrderStatus.COMPLETE.getValue());
				stockMovement.setQuantity(order.getQuantity());

				StockMovement stockMovementLeft = new StockMovement();
				stockMovementLeft.setCreationDate(new Date(Calendar.getInstance().getTime().getTime()));
				stockMovementLeft.setItem(stockMovement.getItem());
				stockMovementLeft.setQuantity(stockQuantity);
				
				stockMovementRepository.save(stockMovementLeft);
			}
			stockMovement.setOrder(order);
		});
		
		sendMessage(order);
		
	}

	private void sendMessage(Order order) {
		if(order.getStatus().equals(OrderStatus.COMPLETE.getValue())) {
			String message = "Hello " + order.getUser().getName()+ "/nWe inform you that your order of " + order.getItem().getName()
					+ " with quantity " + order.getQuantity() + " is complete.";
			emailService.sendEmail(order.getUser().getEmail(), "Status of your order", message);
		}
	}
    
	private void checkOrder(StockMovement stockMovement) {
		List<Order> orders = orderRepository.findByItemNameAndStatus(stockMovement.getItem().getName(),
				OrderStatus.INCOMPLETE.getValue());
		
		StockMovement stockMovementQuantityLeft = new StockMovement();
		stockMovementQuantityLeft.setQuantity(stockMovement.getQuantity());
		stockMovementQuantityLeft.setItem(stockMovement.getItem());
		
		orders.stream().forEach(order -> {
			Long stockAssignedQuantity = order.getStockMovements().stream().mapToLong(StockMovement::getQuantity).sum();

			Long stockQuantity = stockMovementQuantityLeft.getQuantity() + stockAssignedQuantity - order.getQuantity();
			
			if(stockQuantity == 0) {
				order.setStatus(OrderStatus.COMPLETE.getValue());
				stockMovement.setOrder(order);
			}else if(stockQuantity > 0) {
				order.setStatus(OrderStatus.COMPLETE.getValue());
				
				if(stockMovementQuantityLeft.getQuantity() == stockMovement.getQuantity()) {
					stockMovement.setQuantity(order.getQuantity());
					stockMovement.setOrder(order);
				}else {
					saveNewStock(stockMovement, order, order.getQuantity());
				}
				
				stockMovementQuantityLeft.setQuantity(stockQuantity);
			}else {
				if(stockMovementQuantityLeft.getQuantity() == stockMovement.getQuantity()) {
					stockMovement.setOrder(order);
				}else {
					saveNewStock(stockMovementQuantityLeft, order, stockMovementQuantityLeft.getQuantity());
				}
				stockMovementQuantityLeft.setQuantity(stockQuantity);
			}
			
			sendMessage(order);
		});
		
		if(stockMovementQuantityLeft.getQuantity() > 0) {
			saveNewStock(stockMovementQuantityLeft, null, stockMovementQuantityLeft.getQuantity());
		}
		
	}

	private void saveNewStock(StockMovement stockMovement, Order order, Long quantity) {
		StockMovement stockMovementSave = new StockMovement();
		stockMovementSave.setQuantity(quantity);
		stockMovementSave.setOrder(order);
		stockMovementSave.setCreationDate(new Date(Calendar.getInstance().getTime().getTime()));
		stockMovementSave.setItem(stockMovement.getItem());
		
		stockMovementRepository.save(stockMovementSave);
	}
}
