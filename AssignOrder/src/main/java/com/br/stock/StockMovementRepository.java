package com.br.stock;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StockMovementRepository extends JpaRepository<StockMovement, Long>{
	List<StockMovement> findByItemName(String itemName);

	List<StockMovement> findByItemNameAndOrderIsNull(String itemName);
	
	void deleteByItemName(String itemName);
}
