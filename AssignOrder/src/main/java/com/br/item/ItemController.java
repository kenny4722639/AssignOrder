package com.br.item;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/item")
public class ItemController {
	@Autowired
	private ItemService itemService;

	@GetMapping("/get")
	public List<Item> get(@RequestParam(name = "name", required = false) String name) {
        return itemService.get(name);
    }

	@PostMapping("/save")
    public Item save(@RequestBody Item item) {
    	return itemService.save(item);
    }

	@DeleteMapping("/delete")
	public void delete(@RequestParam(name = "name", required = false) String name) {
		itemService.delete(name);
	}
}
