package com.br.item;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItemService {
	@Autowired
	private ItemRepository itemRepository;

	public List<Item> get(String name) {
        return StringUtils.isNotEmpty(name) ? itemRepository.findByName(name) : itemRepository.findAll();
    }

    public Item save(Item item) {
        return itemRepository.save(item);
    }

    public void delete(String name) {
    	List<Item> item = itemRepository.findByName(name);
    	if(!item.isEmpty()) {
    		itemRepository.deleteAll(item);
    	}else {
    		itemRepository.deleteAll();
    	}
    }
}
