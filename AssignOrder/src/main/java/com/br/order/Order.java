package com.br.order;

import java.sql.Date;
import java.util.List;

import com.br.item.Item;
import com.br.stock.StockMovement;
import com.br.user.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "app_order")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Date creationDate;
	@ManyToOne
	private Item item;
	private Long quantity;
	@ManyToOne
	private User user;
	private String status;
	
	@OneToMany(mappedBy = "order")
	@JsonIgnoreProperties(value = {"order"})
    private List<StockMovement> stockMovements;
	
	public Long getId() {
		return id;
	}

	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public Long getQuantity() {
		return quantity;
	}
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public List<StockMovement> getStockMovements() {
		return stockMovements;
	}
}
