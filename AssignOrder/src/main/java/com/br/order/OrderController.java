package com.br.order;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderController {
	@Autowired
	private OrderService orderService;

	@GetMapping("/get")
	public List<Order> get(@RequestParam(name = "name", required = false) String name, 
			@RequestParam(name = "user", required = false) String user) {
        return orderService.get(name, user);
    }

	@PostMapping("/save")
    public Order save(@RequestBody Order Order) {
    	return orderService.save(Order);
    }

	@DeleteMapping("/delete")
	public void delete(@RequestParam(name = "name", required = false) String name, 
			@RequestParam(name = "user", required = false) String user) {
		orderService.delete(name, user);
	}
}
