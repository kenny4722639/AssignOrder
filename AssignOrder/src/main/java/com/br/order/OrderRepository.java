package com.br.order;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>{
	List<Order> findByItemNameAndUserName(String name, String userName);

	List<Order> findByItemNameAndStatus(String name, String status);
	
	void deleteByItemNameAndUserName(String itemName, String userName);
}
