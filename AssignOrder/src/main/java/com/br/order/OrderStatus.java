package com.br.order;

public enum OrderStatus {
	COMPLETE("Complete"), INCOMPLETE("Incomplete");

	private String value;

    private OrderStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
