package com.br.order;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.stock.StockMovementService;

@Service
public class OrderService {
	@Autowired
	private OrderRepository orderRepository;
	@Autowired
	private StockMovementService stockMovementService;

	public List<Order> get(String name, String userName) {
        return StringUtils.isNotEmpty(name) && StringUtils.isNotEmpty(userName) ? 
        		orderRepository.findByItemNameAndUserName(name, userName) : orderRepository.findAll();
    }

    public Order save(Order order) {
    	order.setCreationDate(new Date(Calendar.getInstance().getTime().getTime()));
    	order.setStatus(OrderStatus.INCOMPLETE.getValue());
    	
        order = orderRepository.save(order);
        
        stockMovementService.checkStock(order);
        
        return order;
    }

    public void delete(String itemName, String userName) {
    	List<Order> order = orderRepository.findByItemNameAndUserName(itemName, userName);
    	if(!order.isEmpty()) {
    		orderRepository.deleteAll(order);
    	}else {
    		orderRepository.deleteAll();
    	}
    }
}
