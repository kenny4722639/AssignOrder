package com.br;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssignOrderApplication {
	public static void main(String[] args) {
        SpringApplication.run(AssignOrderApplication.class, args);
    }
}
