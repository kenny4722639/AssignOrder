package com.br.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userService;

	@GetMapping("/get")
	public List<User> get(@RequestParam(name = "name", required = false) String name) {
        return userService.get(name);
    }

	@PostMapping("/save")
    public User save(@RequestBody User user) {
    	return userService.save(user);
    }

	@DeleteMapping("/delete")
	public void delete(@RequestParam(name = "name", required = false) String name) {
		userService.delete(name);
	}
}
