package com.br.user;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;

	public List<User> get(String name) {
        return StringUtils.isNotEmpty(name) ? userRepository.findByName(name) : userRepository.findAll();
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public void delete(String name) {
    	List<User> user = userRepository.findByName(name);
    	if(!user.isEmpty()) {
    		userRepository.deleteAll(user);
    	}else {
    		userRepository.deleteAll();
    	}
    }
}
